from urllib.parse import urlparse, parse_qs
import os, sys
from pathlib import Path
from google_auth_oauthlib.flow import InstalledAppFlow

BASE_DIR = Path(__file__).resolve(strict=True).parent.parent

def create_file(project_id, client_id, client_secret) -> str:

    import json
    client_secret_file_path = os.path.join(BASE_DIR, 'executors', "configs", 'client_secrets.json')
    with open(client_secret_file_path) as fh:
        contents = json.load(fh)
        contents.get(
            'installed', {}
        ).update(
            {
                "client_id": client_id,
                "client_secret": client_secret,
                "project_id": project_id
            }
        )
    real_client_secret_file_path = os.path.join(BASE_DIR, 'executors', "configs", "real_client_secrets.json")
    with open(real_client_secret_file_path, "w") as fh:
        fh.write(json.dumps(contents))
    return real_client_secret_file_path


def run_console_hack(flow):
    flow.redirect_uri = 'http://localhost:1'
    auth_url, _ = flow.authorization_url()
    print(
        "Visit the following URL:",
        auth_url,
        "After granting permissions, you will be redirected to an error page",
        "Copy the URL of that error page (http://localhost:1/?state=...)",
        sep="\n"
    )
    redirect_url = input("URL: ")
    query = urlparse(redirect_url).query
    code = parse_qs(query)['code'][0]
    flow.fetch_token(code=code)
    return flow.credentials



def create_credentials(scopes, client_secrets_file):
    flow = InstalledAppFlow.from_client_secrets_file(
        client_secrets_file=client_secrets_file,
        scopes=scopes,
    )
    creds = run_console_hack(flow)
    with open(os.path.join(BASE_DIR, 'deployment', 'credentials.json'), 'w') as credentials:
        credentials.write(creds.to_json())

if __name__ == '__main__':
    project_id = sys.argv[1]
    client_id = sys.argv[2]
    client_secret = sys.argv[3]

    sys.argv = [sys.argv[0]]

    real_client_secret_file = create_file(project_id, client_id, client_secret)

    create_credentials(scopes=['https://mail.google.com/'], client_secrets_file=real_client_secret_file)
