import os
import json
import requests
import sys

from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent


def delete_applications(auth0_management_uri, bearer_token):
    headers = {
        "Authorization": f"Bearer {bearer_token}",
        "Content-Type": "application/json"
    }
    url = f'{auth0_management_uri}clients'
    res = requests.get(url, headers=headers).json()

    for r in res:
        requests.delete(f'{auth0_management_uri}clients/{r.get("client_id")}', headers=headers)


def delete_apis(auth0_management_uri, bearer_token):
    headers = {
        "Authorization": f"Bearer {bearer_token}",
        "Content-Type": "application/json"
    }
    url = f'{auth0_management_uri}resource-servers'
    res = requests.get(url, headers=headers).json()

    for r in res:
        requests.delete(f'{auth0_management_uri}clients/{r.get("client_id")}', headers=headers)


def create_auth0_resource(auth0_management_uri, bearer_token, json_file, type=None):
    if type is None:
        print('Type is not defined. Must be "api" or "application"')
        return

    if type == "application":
        url = f'{auth0_management_uri}clients'
    elif type == "api":
        url = f'{auth0_management_uri}resource-servers'
    else:
        print("Cannot determine the type. Did you make a typo?")
        return

    headers = {
        "Authorization": f"Bearer {bearer_token}",
        "Content-Type": "application/json"
    }
    with open(json_file) as fh:
        request_body = json.loads(fh.read())
        res = requests.post(url, json=request_body, headers=headers).json()
    return res


def add_roles(auth0_management_uri, bearer_token):
    url = f"{auth0_management_uri}roles"
    headers = {
        "Authorization": f"Bearer {bearer_token}",
        "Content-Type": "application/json"
    }

    roles = [
        {
            "name": "GPD user",
            "description": "GPD user"
        },
        {
            "name": "Super Admin",
            "description": "Super Admin"
        }
    ]
    for role in roles:
        res = requests.post(url, json=role, headers=headers)
        print(res.json())


if __name__ == '__main__':
    auth0_management_uri = sys.argv[1]
    bearer_token = sys.argv[2]
    try:
        delete_applications(bearer_token=bearer_token, auth0_management_uri=auth0_management_uri)
    except Exception as e:
        pass
    try:
        delete_apis(bearer_token=bearer_token, auth0_management_uri=auth0_management_uri)
    except Exception as e:
        pass

    resources = [
        {
            "file": "ga-360-ms-application.json",
            "type": "application"
        },
        {
            "file": "ga-360-ms-api.json",
            "type": "api"
        },
        {
            "file": "gpd-application.json",
            "type": "application"
        },
        {
            "file": "gpd-api.json",
            "type": "api"
        }
    ]

    for resource in resources:
        file_path = os.path.join(BASE_DIR, 'executors', 'configs', resource.get('file'))
        res = create_auth0_resource(
            auth0_management_uri=auth0_management_uri,
            bearer_token=bearer_token,
            json_file=file_path,
            type=resource.get('type')
        )
        print(res)

    add_roles(
        auth0_management_uri=auth0_management_uri,
        bearer_token=bearer_token
    )
