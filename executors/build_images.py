import os
import sys
import tarfile
import subprocess

from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent


def execute_command(command: str):
    process = subprocess.Popen(
        command,
        stdout=subprocess.PIPE,
        universal_newlines=True,
        shell=True
    )

    while True:
        output = process.stdout.readline()
        print(output.strip())
        # Do something else
        return_code = process.poll()
        if return_code is not None:
            # Process has finished, read rest of the output
            for output in process.stdout.readlines():
                print(output.strip())
            break


def get_image_info(registry_uri, project_id):
    return [

        {
            'docker_image_tag': f'{registry_uri}/{project_id}/gpd-web:latest',
            'zip': 'seamless-precision-dashboard-master.tar.gz',
            'unpack_folder': 'seamless-precision-dashboard-master'
        },
        {
            'docker_image_tag': f'{registry_uri}/{project_id}/gpd-api:latest',
            'zip': 'seamless-precision-dashboard-master.tar.gz',
            'unpack_folder': 'seamless-precision-dashboard-master'
        },
        {
            'docker_image_tag': f'{registry_uri}/{project_id}/ga360-ms-api:latest',
            'zip': 'ga360-api-microservice-master.tar.gz',
            'unpack_folder': 'ga360-api-microservice-master'
        },
        {
            'docker_image_tag': f'{registry_uri}/{project_id}/sa360-report-ms:latest',
            'zip': 'sa-360-report-microservice-master.tar.gz',
            'unpack_folder': 'sa-360-report-microservice-master'
        }

    ]


if __name__ == '__main__':
    registry_uri = sys.argv[1]
    project_id = sys.argv[2]

    image_infos = get_image_info(registry_uri=registry_uri, project_id=project_id)
    for image_info in image_infos:
        storepath = os.path.join(BASE_DIR, 'data')
        # Download the source files and store them in the data dr
        execute_command(f'gsutil cp gs://gpd-source-files/{image_info.get("zip")} {storepath}')
        # Extract files from tar-file
        tf = tarfile.open(os.path.join(storepath, image_info.get("zip")))

        tf.extractall(os.path.join(storepath, image_info.get("unpack_folder")))

        if 'gpd-web' in image_info.get('docker_image_tag'):
            execute_command(
                f'cp {os.path.join(BASE_DIR, "auth_config.json")} {os.path.join(storepath, image_info.get("unpack_folder"))}/web'
            )
            execute_command(
                f'cd {os.path.join(storepath, image_info.get("unpack_folder"))}/web && npm install'
            )
            execute_command(
                f'cd {os.path.join(storepath, image_info.get("unpack_folder"))}/web && npm run build'
            )
            execute_command(
                f'gcloud builds submit  --tag {image_info.get("docker_image_tag")} {os.path.join(storepath, image_info.get("unpack_folder"))}/web --project={project_id}'
            )

        elif 'gpd-api' in image_info.get('docker_image_tag'):
            execute_command(
                f'gcloud builds submit  --tag {image_info.get("docker_image_tag")} {os.path.join(storepath, image_info.get("unpack_folder"))}/app --project={project_id}'
            )

        else:
            execute_command(
                f'gcloud builds submit  --tag {image_info.get("docker_image_tag")} {os.path.join(storepath, image_info.get("unpack_folder"))} --project={project_id}'
            )
