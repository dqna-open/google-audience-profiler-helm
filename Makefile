# Please fill in the env vars in the .env
# before you begin
include .env
export

cloud_login:
	# Please use an account that has rights to create a new project
	# and has the permission "gcloud.alpha.billing.projects.link".
	# If this is not the case, you cannot complete the installation process
	gcloud auth login

create_project:
	-gcloud projects create ${PROJECT_ID}
	-gcloud config set project ${PROJECT_ID}

link_billing_account:
	-gcloud alpha billing projects link ${PROJECT_ID} --billing-account ${BILLING_ACCOUNT}

create_buckets:
	-gsutil mb -p ${PROJECT_ID} -l ${BUCKET_LOCATION} gs://${PROJECT_ID}-${SA_360_OUTPUT_BUCKET}
	-gsutil mb -p ${PROJECT_ID} -l ${BUCKET_LOCATION} gs://${PROJECT_ID}-${GA_360_OUTPUT_BUCKET}
	-gsutil mb -p ${PROJECT_ID} -l ${BUCKET_LOCATION} gs://${PROJECT_ID}-${GA_4_OUTPUT_BUCKET}

create_service_account:
	-gcloud config set project ${PROJECT_ID}
	-gcloud iam service-accounts create ${SERVICE_ACCOUNT_NAME} --display-name="Audience Profiler Service Account"
	-gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  		--member=serviceAccount:${SERVICE_ACCOUNT_NAME}@${PROJECT_ID}.iam.gserviceaccount.com \
		--role=roles/storage.admin

create_service_account_key:
	-gcloud iam service-accounts keys create deployment/sak.json \
		--iam-account=${SERVICE_ACCOUNT_NAME}@${PROJECT_ID}.iam.gserviceaccount.com

enable_apis:
	# Enabling all the required APIs.
	# This can take some time. Please be patient
	-gcloud services enable container.googleapis.com
	-gcloud services enable storage.googleapis.com
	-gcloud services enable analytics.googleapis.com
	-gcloud services enable analyticsdata.googleapis.com
	-gcloud services enable gmail.googleapis.com
	-gcloud services enable cloudbuild.googleapis.com
	-gcloud services enable sheets.googleapis.com

the_cluster:
	# Create a 1-node cluster in the project and zone defined in the .env file
	@-gcloud container --project ${PROJECT_ID} clusters create ${CLUSTER_NAME} \
    	--zone ${CLUSTER_ZONE} \
    	--machine-type e2-medium \
    	--image-type COS \
    	--num-nodes 1 \
    	--no-enable-ip-alias

the_auth0_configurations:
	@python3 ./executors/auth0.py ${AUTH0_MANAGEMENT_URI} ${AUTH0_MANAGEMENT_BEARER_TOKEN}

the_sources:
	@python3 ./executors/build_images.py ${DOCKER_IMAGE_REPO} ${PROJECT_ID}

the_sa360_configurations:
	@python3 ./executors/create_credentials.py \
		${PROJECT_ID} \
		${SA_360_GOOGLE_CLIENT_ID} \
		${SA_360_GOOGLE_CLIENT_SECRET}

install_application_dry_run:
	gcloud config set project ${PROJECT_ID}
	gcloud container clusters get-credentials ${CLUSTER_NAME} --zone ${CLUSTER_ZONE} --project ${PROJECT_ID}
	-kubectl create namespace ${NAMESPACE}
	helm install -n ${NAMESPACE} audience-profiler-tool ./deployment \
		--set namespace=${NAMESPACE} \
		--set image_root=${IMAGE_REPO_URL} \
		--set secrets.db_name=${DB_NAME} \
		--set secrets.db_user=${DB_USER} \
		--set secrets.db_password=${DB_PASSWORD} \
		--set env_vars.auth0_tenant_id=${AUTH0_TENANT_ID} \
		--set env_vars.auth0_client_id_management=${AUTH0_CLIENT_ID_MANAGEMENT} \
		--set env_vars.auth0_client_secret_management=${AUTH0_CLIENT_SECRET_MANAGEMENT} \
		--set env_vars.auth0_client_id_ga360_ms_m2m=${AUTH0_CLIENT_ID_GA360_MS_M2M} \
		--set env_vars.auth0_client_secret_ga360_ms_m2m=${AUTH0_CLIENT_SECRET_GA360_MS_M2M} \
		--set env_vars.ga_360_ms_endpoint=${GA_360_MS_ENDPOINT} \
		--set env_vars.bucket_name=gs://${PROJECT_ID}-${SA_360_OUTPUT_BUCKET} \
		--set env_vars.sa360_bucket_name=${PROJECT_ID}-${SA_360_OUTPUT_BUCKET} \
		--set env_vars.ga360_bucket_name=${PROJECT_ID}-${GA_360_OUTPUT_BUCKET} \
		--set env_vars.ga4_bucket_name=${PROJECT_ID}-${GA4_BUCKET_NAME} \
		--set env_vars.google_client_id=${GOOGLE_CLIENT_ID} \
		--set env_vars.google_client_secret=${GOOGLE_CLIENT_SECRET} \
		--dry-run

install_or_upgrade_application:
	gcloud config set project ${PROJECT_ID}
	gcloud container clusters get-credentials ${CLUSTER_NAME} --zone ${CLUSTER_ZONE} --project ${PROJECT_ID}
	-kubectl create namespace ${NAMESPACE}
	helm upgrade -i audience-profiler-tool -n ${NAMESPACE} ./deployment \
		--set namespace=${NAMESPACE} \
		--set image_root=${IMAGE_REPO_URL} \
		--set secrets.db_name=${DB_NAME} \
		--set secrets.db_user=${DB_USER} \
		--set secrets.db_password=${DB_PASSWORD} \
		--set env_vars.auth0_tenant_id=${AUTH0_TENANT_ID} \
		--set env_vars.auth0_client_id_management=${AUTH0_CLIENT_ID_MANAGEMENT} \
		--set env_vars.auth0_client_secret_management=${AUTH0_CLIENT_SECRET_MANAGEMENT} \
		--set env_vars.auth0_client_id_ga360_ms_m2m=${AUTH0_CLIENT_ID_GA360_MS_M2M} \
		--set env_vars.auth0_client_secret_ga360_ms_m2m=${AUTH0_CLIENT_SECRET_GA360_MS_M2M} \
		--set env_vars.ga_360_ms_endpoint=${GA_360_MS_ENDPOINT} \
		--set env_vars.bucket_name=gs://${PROJECT_ID}-${SA_360_OUTPUT_BUCKET} \
		--set env_vars.sa360_bucket_name=${PROJECT_ID}-${SA_360_OUTPUT_BUCKET} \
		--set env_vars.ga360_bucket_name=${PROJECT_ID}-${GA_360_OUTPUT_BUCKET} \
		--set env_vars.ga4_bucket_name=${PROJECT_ID}-${GA4_BUCKET_NAME} \
		--set env_vars.google_client_id=${GOOGLE_CLIENT_ID} \
		--set env_vars.google_client_secret=${GOOGLE_CLIENT_SECRET}

#--------------------------------
# Optional Commands
#--------------------------------

# Completely remove the application
uninstall_application:
	gcloud container clusters get-credentials ${CLUSTER_NAME} --zone ${CLUSTER_ZONE} --project ${PROJECT_ID}
	helm uninstall -n ${NAMESPACE} audience-profiler-tool
